﻿

#pragma checksum "C:\Users\S. Jacob Powell\Documents\My Documents\Programming\Visual Studio\Bulls and Cows\Bulls and Cows\MainPage - Copy.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "67C94B2F95D948C90EA639D55ECDD626"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bulls_and_Cows
{
    partial class MainPage : global::Windows.UI.Xaml.Controls.Page
    {
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button optionsButton; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.ListBox guesses; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.StackPanel guessPanel; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.StackPanel charmsPanel1; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.StackPanel charmsPanel2; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image makeAGuess; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image greenCircle; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image yellowCircle; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Primitives.Popup winPopUp; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.MediaElement sounds; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBlock winPopUpTextBlock; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image jigsaw5; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image crown6; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image star7; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image water8; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image shovel9; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image spiral0; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image ribbon1; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image lightning2; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image fire3; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image heart4; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private bool _contentLoaded;

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent()
        {
            if (_contentLoaded)
                return;

            _contentLoaded = true;
            global::Windows.UI.Xaml.Application.LoadComponent(this, new global::System.Uri("ms-appx:///MainPage - Copy.xaml"), global::Windows.UI.Xaml.Controls.Primitives.ComponentResourceLocation.Application);
 
            optionsButton = (global::Windows.UI.Xaml.Controls.Button)this.FindName("optionsButton");
            guesses = (global::Windows.UI.Xaml.Controls.ListBox)this.FindName("guesses");
            guessPanel = (global::Windows.UI.Xaml.Controls.StackPanel)this.FindName("guessPanel");
            charmsPanel1 = (global::Windows.UI.Xaml.Controls.StackPanel)this.FindName("charmsPanel1");
            charmsPanel2 = (global::Windows.UI.Xaml.Controls.StackPanel)this.FindName("charmsPanel2");
            makeAGuess = (global::Windows.UI.Xaml.Controls.Image)this.FindName("makeAGuess");
            greenCircle = (global::Windows.UI.Xaml.Controls.Image)this.FindName("greenCircle");
            yellowCircle = (global::Windows.UI.Xaml.Controls.Image)this.FindName("yellowCircle");
            winPopUp = (global::Windows.UI.Xaml.Controls.Primitives.Popup)this.FindName("winPopUp");
            sounds = (global::Windows.UI.Xaml.Controls.MediaElement)this.FindName("sounds");
            winPopUpTextBlock = (global::Windows.UI.Xaml.Controls.TextBlock)this.FindName("winPopUpTextBlock");
            jigsaw5 = (global::Windows.UI.Xaml.Controls.Image)this.FindName("jigsaw5");
            crown6 = (global::Windows.UI.Xaml.Controls.Image)this.FindName("crown6");
            star7 = (global::Windows.UI.Xaml.Controls.Image)this.FindName("star7");
            water8 = (global::Windows.UI.Xaml.Controls.Image)this.FindName("water8");
            shovel9 = (global::Windows.UI.Xaml.Controls.Image)this.FindName("shovel9");
            spiral0 = (global::Windows.UI.Xaml.Controls.Image)this.FindName("spiral0");
            ribbon1 = (global::Windows.UI.Xaml.Controls.Image)this.FindName("ribbon1");
            lightning2 = (global::Windows.UI.Xaml.Controls.Image)this.FindName("lightning2");
            fire3 = (global::Windows.UI.Xaml.Controls.Image)this.FindName("fire3");
            heart4 = (global::Windows.UI.Xaml.Controls.Image)this.FindName("heart4");
        }
    }
}



