﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace Bulls_and_Cows
{
	public sealed partial class rulesPage : Page
	{
		Uri guessReadySound = new Uri("ms-appx:Assets/Sounds/guessReady.wav", UriKind.Absolute);
		Uri bull1 = new Uri("ms-appx:Assets/Sounds/bull1.wav", UriKind.Absolute);
		Uri cleot1 = new Uri("ms-appx:Assets/Sounds/cleot1.wav", UriKind.Absolute);
		bool easyModeOn, originalMode;

		public rulesPage()
		{
			this.InitializeComponent();
		}

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			try
			{
				toggleSwitch.IsOn = (bool)e.Parameter;
				playTheGameButton.Content = "Resume Game";
			}
			catch
			{
				toggleSwitch.IsOn = false;
				playTheGameButton.Content = "Start Game";
			}
			easyModeOn = toggleSwitch.IsOn;
			originalMode = easyModeOn;
			modePopUp.IsOpen = false;
			playTheGameButton.IsEnabled = true;
			toggleSwitch.IsEnabled = true;
			changePicturesToMode(toggleSwitch.IsOn);
		}

		private void playSound(Uri uri)
		{
			sounds.Source = uri;

			if (sounds.CurrentState == MediaElementState.Playing)
				sounds.Position = new TimeSpan(0);
			else
				sounds.Play();
		} 
		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Frame.Navigate(typeof(MainPage), toggleSwitch.IsOn);
		}

		private void makeAGuessTap(object sender, RoutedEventArgs e)
		{
			playSound(guessReadySound);
		}

		private void bullTap(object sender, TappedRoutedEventArgs e)
		{
			playSound(bull1);
		}

		private void cleotTap(object sender, TappedRoutedEventArgs e)
		{
			playSound(cleot1);
		}

		private void toggleSwitch_Toggled(object sender, RoutedEventArgs e)
		{
			changePicturesToMode(toggleSwitch.IsOn);
			if (toggleSwitch.IsOn == originalMode)
				return;
			modePopUp.IsOpen = true;
			playTheGameButton.IsEnabled = false;
			toggleSwitch.IsEnabled = false;
		}

		private void changePicturesToMode(bool easyModeIsOn)
		{
			if(easyModeIsOn)
			{
				pic1.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/zero0.png", UriKind.Absolute));
				pic2.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/seven7.png", UriKind.Absolute));
				pic3.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/four4.png", UriKind.Absolute));
				pic4.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/eight8.png", UriKind.Absolute));
				pic5.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/two2.png", UriKind.Absolute));
			}
			else
			{
				pic1.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/spirals3.png", UriKind.Absolute));
				pic2.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/star129.png", UriKind.Absolute));
				pic3.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/loving4.png", UriKind.Absolute));
				pic4.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/water81.png", UriKind.Absolute));
				pic5.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/eco7.png", UriKind.Absolute));
			}
			pic2clone.Source = pic2.Source;
			pic3clone.Source = pic3.Source;
			pic4clone.Source = pic4.Source;
		}

		private void noButtonClick(object sender, RoutedEventArgs e)
		{
			toggleSwitch.IsOn = !toggleSwitch.IsOn;
			modePopUp.IsOpen = false;
			playTheGameButton.IsEnabled = true;
			toggleSwitch.IsEnabled = true;
		}

		private void yesButtonClick(object sender, RoutedEventArgs e)
		{
			modePopUp.IsOpen = false;
			playTheGameButton.IsEnabled = true;
			toggleSwitch.IsEnabled = true;
			playTheGameButton.Content = "Start Game";
		}
	}
}
