﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.Phone.UI.Input;

namespace Bulls_and_Cows
{
    public sealed partial class MainPage : Page
    {
		string secretNumber = "";
		int guessesHeight = 30;
		bool win;

		List<string> guessArray = new List<string>();

		Uri newGameSound = new Uri("ms-appx:Assets/Sounds/newGame.wav", UriKind.Absolute);
		Uri winSound = new Uri("ms-appx:Assets/Sounds/win.wav", UriKind.Absolute);
		Uri guessReadySound = new Uri("ms-appx:Assets/Sounds/guessReady.wav", UriKind.Absolute);
		Uri gradeEmptySound = new Uri("ms-appx:Assets/Sounds/gradeEmpty.wav", UriKind.Absolute);
		Uri addToGuessSound = new Uri("ms-appx:Assets/Sounds/addToGuess.wav", UriKind.Absolute);
		Uri alreadyGuessedSound = new Uri("ms-appx:Assets/Sounds/alreadyGuessed.wav", UriKind.Absolute);

		Uri bull1 = new Uri("ms-appx:Assets/Sounds/bull1.wav", UriKind.Absolute);
		Uri bull1cleot1 = new Uri("ms-appx:Assets/Sounds/bull1cleot1.wav", UriKind.Absolute);
		Uri bull1cleot2 = new Uri("ms-appx:Assets/Sounds/bull1cleot2.wav", UriKind.Absolute);
		Uri bull1cleot3 = new Uri("ms-appx:Assets/Sounds/bull1cleot3.wav", UriKind.Absolute);

		Uri bull2 = new Uri("ms-appx:Assets/Sounds/bull2.wav", UriKind.Absolute);
		Uri bull2cleot1 = new Uri("ms-appx:Assets/Sounds/bull2cleot1.wav", UriKind.Absolute);
		Uri bull2cleot2 = new Uri("ms-appx:Assets/Sounds/bull2cleot2.wav", UriKind.Absolute);

		Uri bull3 = new Uri("ms-appx:Assets/Sounds/bull3.wav", UriKind.Absolute);
		Uri bull3cleot1 = new Uri("ms-appx:Assets/Sounds/bull3cleot1.wav", UriKind.Absolute);

		Uri cleot1 = new Uri("ms-appx:Assets/Sounds/cleot1.wav", UriKind.Absolute);
		Uri cleot2 = new Uri("ms-appx:Assets/Sounds/cleot2.wav", UriKind.Absolute);
		Uri cleot3 = new Uri("ms-appx:Assets/Sounds/cleot3.wav", UriKind.Absolute);
		Uri cleot4 = new Uri("ms-appx:Assets/Sounds/cleot4.wav", UriKind.Absolute);

		Windows.Storage.ApplicationDataContainer stats = Windows.Storage.ApplicationData.Current.LocalSettings;

		string numberOfStarts, numberOfFinishes, totalNumberOfGuesses;

		bool easyModeOn;

        public MainPage()
        {
            this.InitializeComponent();
			////HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

		private void incrementStat(string statName)
		{
			stats.Values[statName] = ((int)stats.Values[statName]) + 1;
			setStats();
		}

		private void updateAverageStat(int guessCount)
		{
			stats.Values[totalNumberOfGuesses] = ((int)stats.Values[totalNumberOfGuesses]) + guessCount;
			setStats();
		}

		private void initStats()
		{
			if (stats.Values[numberOfFinishes] == null)
			{
				stats.Values[numberOfFinishes] = 0;
			}

			if (stats.Values[numberOfStarts] == null)
			{
				stats.Values[numberOfStarts] = 0;
			}

			if (stats.Values[totalNumberOfGuesses] == null)
			{
				stats.Values[totalNumberOfGuesses] = 0;
			}
			setStats();
		}

		private void setStats()
		{
			gamesFinished.Text = stats.Values[numberOfFinishes].ToString();

			gamesStarted.Text = stats.Values[numberOfStarts].ToString();

			try
			{
				averageGuessCount.Text = ((double)((((int)stats.Values[totalNumberOfGuesses]) / ((int)stats.Values[numberOfFinishes])))).ToString();
			}
			catch
			{
				averageGuessCount.Text = "-";
			}
		}


		private void newGame()
		{
			incrementStat(numberOfStarts);
			playSound(newGameSound);
			guesses.Items.Clear();
			winPopUp.IsOpen = false;
			win = false;
			secretNumber = getNewNumber();
			guessArray = new List<string>();
			returnGuessToCharmsPanels();
			if (easyModeOn)
				winPopUpTextBlock.Text = "Easy Mode (Numbers)";
			else
				winPopUpTextBlock.Text = "Normal Mode (Charms)";

		}

		//private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
		//{
		//	e.Handled = true;
		//	winPopUp.IsOpen = false;
		//	Frame.Navigate(typeof(rulesPage), easyModeOn);
		//}
		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			if ((bool)e.Parameter)
			{
				numberOfStarts = "numberOfStartsEasy";
				numberOfFinishes = "numberOfFinishesEasy";
				totalNumberOfGuesses = "totalNumberOfGuessesEasy";
				spiral0.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/zero0.png", UriKind.Absolute));
				ribbon1.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/one1.png", UriKind.Absolute));
				lightning2.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/two2.png", UriKind.Absolute));
				fire3.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/three3.png", UriKind.Absolute));
				heart4.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/four4.png", UriKind.Absolute));
				jigsaw5.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/five5.png", UriKind.Absolute));
				crown6.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/six6.png", UriKind.Absolute));
				star7.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/seven7.png", UriKind.Absolute));
				water8.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/eight8.png", UriKind.Absolute));
				shovel9.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/nine9.png", UriKind.Absolute));
			}
			else
			{
				numberOfStarts = "numberOfStarts";
				numberOfFinishes = "numberOfFinishes";
				totalNumberOfGuesses = "totalNumberOfGuesses";
				spiral0.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/spirals3.png", UriKind.Absolute));
				ribbon1.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/christmas151.png", UriKind.Absolute));
				lightning2.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/eco7.png", UriKind.Absolute));
				fire3.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/flame7.png", UriKind.Absolute));
				heart4.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/loving4.png", UriKind.Absolute));
				jigsaw5.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/puzzle38.png", UriKind.Absolute));
				crown6.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/royal162.png", UriKind.Absolute));
				star7.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/star129.png", UriKind.Absolute));
				water8.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/water81.png", UriKind.Absolute));
				shovel9.Source = new BitmapImage(new Uri("ms-appx:Assets/Pictures/gardening15.png", UriKind.Absolute));
			}
			initStats();
			if (((bool)e.Parameter) != easyModeOn || (int)stats.Values[numberOfStarts] == 0 || guesses.Items.Count == 0)
				newGame();


			if(secretNumber == "")
				secretNumber = getNewNumber();
			easyModeOn = ((bool)e.Parameter);

			if (easyModeOn)
				winPopUpTextBlock.Text = "Easy Mode (Numbers)";
			else
				winPopUpTextBlock.Text = "Normal Mode (Charms)";
		}

		private Image createBullOrCleot(bool bull)
		{
			Image bullOrCleot = new Image();
			bullOrCleot.Width = 15;
			bullOrCleot.Height = guessesHeight;
			bullOrCleot.IsTapEnabled = false;

			if (bull)
				bullOrCleot.Source = greenCircle.Source;
			else
				bullOrCleot.Source = yellowCircle.Source;


				return bullOrCleot;
		}

        private string getNewNumber()
		{
			Random rand = new Random();
			string newNumber = rand.Next(10).ToString();

			string currentAddition;
			while(newNumber.Length != 4)
			{
				currentAddition = rand.Next(10).ToString();
				if (!newNumber.Contains(currentAddition))
					newNumber += currentAddition;
			}

			return newNumber;
        }

		private void checkIfWin(int bullCount)
		{
			if (bullCount == 4)
			{
				playSound(winSound);
				int guessCount = guesses.Items.Count + 1;
				winPopUpTextBlock.Text = "You win!\nIt took " + guessCount.ToString() + " " + (guessCount == 1 ? "guess" : "guesses");
				incrementStat(numberOfFinishes);
				updateAverageStat(guessCount);
				winPopUp.IsOpen = true;
				win = true;
			}
		}


		private void returnGuessToCharmsPanels()
		{
			while (guessPanel.Children.Count != 0)
			{
				try
				{
					guessOrRemoveCharm(guessPanel.Children[0] as Image);
				}
				catch
				{
					break;
				}
			}

		}

		private StackPanel addGrade(StackPanel guess)
		{
			string charmID;
			int bullCount = 0, cleotCount = 0;


			StackPanel gradePanel = new StackPanel();
			gradePanel.Orientation = Orientation.Horizontal;
			MediaElementState currentState = sounds.CurrentState;

			for (int i = 0; i < 4; i++)
			{
				charmID = getCharmID(guess.Children[i] as Image).ToString();

				if (secretNumber.Contains(charmID))
				{
					if (secretNumber.IndexOf(charmID) == guess.Children.IndexOf(guess.Children[i]))
					{
						bullCount++;
						currentState = sounds.CurrentState;
						gradePanel.Children.Insert(0, createBullOrCleot(true));
					}
					else
					{
						cleotCount++;
						currentState = sounds.CurrentState;
						gradePanel.Children.Add(createBullOrCleot(false));
					}
				}
			}
			checkIfWin(bullCount);

			returnGuessToCharmsPanels();

			if (gradePanel.Children.Count == 0)
				playSound(gradeEmptySound);
			else
				playBullOrCleot(bullCount, cleotCount);

			guess.Children.Add(gradePanel);

			return guess;
		}
		private void playBullOrCleot(int bullCount, int cleotCount)
		{
			if(bullCount == 0)
			{
				if(cleotCount == 1)
					playSound(cleot1);
				else
				if(cleotCount == 2)
					playSound(cleot2);
				else
				if(cleotCount == 3)
					playSound(cleot3);
				else
				if(cleotCount == 4)
					playSound(cleot4);
			}
			else
			if(bullCount == 1)
			{
				if(cleotCount == 0)
					playSound(bull1);
				else
				if(cleotCount == 1)
					playSound(bull1cleot1);
				else
				if(cleotCount == 2)
					playSound(bull1cleot2);
				else
				if(cleotCount == 3)
					playSound(bull1cleot3);

			}
			else
			if(bullCount == 2)
			{
				if (cleotCount == 0)
					playSound(bull2);
				else
				if (cleotCount == 1)
					playSound(bull2cleot1);
				else
				if (cleotCount == 2)
					playSound(bull2cleot2);
			}
			else
			if (bullCount == 3)
			{
				if (cleotCount == 0)
					playSound(bull3);
				else
				if (cleotCount == 1)
					playSound(bull3cleot1);
			}
		}
		private void playSound(Uri uri)
		{
			sounds.Source = uri;

			if (sounds.CurrentState == MediaElementState.Playing)
				sounds.Position = new TimeSpan(0);
			else
				sounds.Play();
		}
		private Image getCopy(Image currentImage, string newName)
		{
			Image newImage = new Image();
			newImage.Source = currentImage.Source;
			newImage.Name = "guess" + guesses.Items.Count.ToString() + "_" + getCharmID(currentImage);
			newImage.Width = guessesHeight;
			newImage.Height = guessesHeight;
			newImage.HorizontalAlignment = currentImage.HorizontalAlignment;
			newImage.VerticalAlignment = currentImage.VerticalAlignment;
			return newImage;
		}
		private void addToGuessList(object sender, RoutedEventArgs e)
		{
			if(!guessReady())
				return;

			StackPanel guess = new StackPanel();
			guess.Orientation = Orientation.Horizontal;
			Image currentImage;
			for (int i = 0; i < 4; i++)
			{
				currentImage = guessPanel.Children[i] as Image;

				guess.Children.Add(getCopy(currentImage, "guess" + guesses.Items.Count.ToString() + "_" + getCharmID(currentImage)));
			}

			guessArray.Add(getGuessNumberString());

			guesses.Items.Add(addGrade(guess));


			guesses.UpdateLayout();
			guesses.SelectedIndex = guesses.Items.Count - 1;
			guesses.ScrollIntoView(guesses.SelectedItem);

		}

		private void optionsButtonClick(object sender, RoutedEventArgs e)
		{
			setStats();
			winPopUp.IsOpen = true;
		}

		private void newGameButtonClick(object sender, RoutedEventArgs e)
		{
			newGame();
		}

		private int getCharmID(Image image)
		{
			if (image != null)
			{
				if (image.Name.Length == 0)
				{
					foreach (object child in charmsPanel1.Children)
					{
						if (((Image)child).Source == image.Source)
							return getCharmID(image);
					}
					foreach (object child in charmsPanel2.Children)
					{
						if (((Image)child).Source == image.Source)
							return getCharmID(image);
					}
				}
				else
					return (int)image.Name.ToString()[image.Name.Length - 1] - 48;
			}
			return 0;
		}
		private string getGuessNumberString()
		{
			string currentGuess = "";

			for (int i = 0; i < 4; i++)
				currentGuess += getCharmID(guessPanel.Children[i] as Image).ToString();

			return currentGuess;
		}
		private bool alreadyGuessed()
		{
			if (guessArray.Contains(getGuessNumberString()))
			{
				playSound(alreadyGuessedSound);
				return true;
			}
			else
				return false;
		}
		private bool guessReady()
		{
			if (guessPanel.Children.Count == 4)
			{
				if (alreadyGuessed())
					return true;
				playSound(guessReadySound);
				makeAGuess.Visibility = Windows.UI.Xaml.Visibility.Visible;
				makeAGuess.IsTapEnabled = true;
				return true;
			}
			else
			{
				makeAGuess.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
				makeAGuess.IsTapEnabled = false;
				return false;
			}
		}

		private void Image_Tapped(object sender, TappedRoutedEventArgs e)
		{
			guessOrRemoveCharm(sender as Image);
		}

		private void guessOrRemoveCharm(Image movingImage)
		{
			StackPanel parent = movingImage.Parent as StackPanel;
			if (parent == guessPanel)
			{
				int index = getCharmID(movingImage);
				guessPanel.Children.Remove(movingImage);
				StackPanel currentPanel = index < 5 ? charmsPanel1 : charmsPanel2;
				index = index < 5 ? index : index - 5;
				((Image)currentPanel.Children[index]).Source = movingImage.Source;

			}
			else
			if (!win)
			{
				if (guessReady())
					return;
				playSound(addToGuessSound);
				Image newImage = getCopy(movingImage, "guess" + guessPanel.Children.Count.ToString());
				movingImage.Source = null;
				newImage.IsTapEnabled = true;
				newImage.Tapped += Image_Tapped;
				newImage.Height = 50;
				newImage.Width = 50;
				guessPanel.Children.Add(newImage);
			}

			guessReady();

		}

		private void popUpCancel(object sender, RoutedEventArgs e)
		{
			winPopUp.IsOpen = false;
		}

		private void soundsCurrentStateChanged(object sender, RoutedEventArgs e)
		{
			if (sounds.CurrentState == MediaElementState.Paused)
				sounds.Stop();
		}

		private void rulesButtonClick(object sender, RoutedEventArgs e)
		{
			winPopUp.IsOpen = false;
			Frame.Navigate(typeof(rulesPage), easyModeOn);
		}

	}
}

